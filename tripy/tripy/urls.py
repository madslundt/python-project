from django.conf.urls import patterns, include, url
from django.contrib import admin

from content import views as content_views

urlpatterns = patterns('',
    url(r'^$', content_views.home, name='content.home'),

    url(r'^get_map_places$', content_views.get_map_places, name='content.get_map_places'),
    url(r'^get_machinelearning_data$', content_views.get_machinelearning_data, name='content.get_machinelearning_data'),
    url(r'^get_reviews_data$', content_views.get_reviews_data, name='content.get_reviews_data'),
    url(r'^get_network$', content_views.get_network, name='content.get_network'),
    url(r'^get_network2$', content_views.get_network2, name='content.get_network2'),
    url(r'^get_rating_errors$', content_views.get_rating_errors, name='content.get_rating_errors'),
    url(r'^get_review_rating_distribution$', content_views.get_review_rating_distribution, name='content.get_review_rating_distribution'),
    url(r'^get_place_rating_distribution$', content_views.get_place_rating_distribution, name='content.get_place_rating_distribution'),
    
    
    url(r'^robots.txt/$', content_views.robots, name='content.robots'),

    url(r'^about/$', content_views.about, name='content.about'),

    url(r'^admin/', include(admin.site.urls)),
)
