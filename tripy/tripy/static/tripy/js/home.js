var map_functions = {
    map: false, heatmap: new google.maps.visualization.HeatmapLayer(), bounds: [], markers: [], bounds: new google.maps.LatLngBounds(), heatMapData: [],
    init: function(lat, lng) {
        this.getPlaces();
        this.getMap(lat, lng);
        //this.changeHeatmapGradient();
        this.toggleHeatmap();
        this.toggleMenu();
        this.removeMenu();
        this.skrollr();
        this.changeSearchBUtton();
        this.apitabs();
        this.visualizationtabs();
        this.changeHeatmapRadius(10);
        $('.masthead .loader').fadeIn();
    },
    getPlaces: function() {
        $.ajax({
            url: get_map_places_url,
            data: {
                'query': query
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                for (i in data.results) {
                    map_functions.heatMapData.push({
                        location: new google.maps.LatLng(data.results[i].geo_lat, data.results[i].geo_lng), 
                        weight: Math.floor(data.results[i].new_avg_rating)
                    });
                    map_functions.addMarker(new google.maps.LatLng(data.results[i].geo_lat, data.results[i].geo_lng, i));
                }
                map_functions.map.fitBounds(map_functions.bounds);
                setTimeout(function () {
                    $('.masthead .loader').fadeOut();
                }, 500);
            },
            error: function(data) { 
                global_functions.raise_alert("Collecting places for the heatmap");
                $('.masthead .loader').fadeOut();
            }
        });

    },
    getMap: function(latitude, longitude) {
        map_functions.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: new google.maps.LatLng(latitude, longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        });
        if (locations.length > 0) {
            var i;
            for (i = 0; i < locations.length; i++) {
                addMarker(new google.maps.LatLng(locations[i][1], locations[i][2]), i);
            }
            map_functions.map.fitBounds(map_functions.bounds);
        }
        if (map_functions.heatMapData) {
            var pointArray = new google.maps.MVCArray(map_functions.heatMapData);
            map_functions.heatmap = new google.maps.visualization.HeatmapLayer({
                max: 50,
                data: pointArray
            });

            map_functions.heatmap.setMap(null);
        }
    },
    addMarker: function(location, i) {
        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: location,
            map: map_functions.map
        });
        map_functions.bounds.extend(marker.getPosition());
        /*google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));*/
        map_functions.markers.push(marker);
    },
    setAllMap: function(map) {
        for (var i = 0; i < map_functions.markers.length; i++) {
            map_functions.markers[i].setMap(map);
        }
    },
    clearMarkers: function() {
        map_functions.setAllMap(null);
    },
    showMarkers: function() {
        map_functions.setAllMap(map_functions.map);
    },
    toggleHeatmap: function() {
        $('.js-heatmap').click(function () {
            if (map_functions.heatmap.getMap()) {
                $(this).children('i').removeClass('glyphicon-map-marker');
                $(this).children('i').addClass('glyphicon-fire');
                map_functions.heatmap.setMap(null);
                map_functions.showMarkers();
            } else {
                $(this).children('i').removeClass('glyphicon-fire');
                $(this).children('i').addClass('glyphicon-map-marker');
                map_functions.heatmap.setMap(map_functions.map);
                map_functions.clearMarkers();
            }
            return false;
        });
    },
    changeHeatmapGradient: function() {
        var gradient = [
            'rgba(102,255,0,0)', 
             'rgba(147,255,0,1)', 
             'rgba(238,255,0,1)',  
             'rgba(249,198,0,1)',  
             'rgba(255,113,0,1)', 
             'rgba(255,0,0,1)'
          ]
          this.heatmap.set('gradient', gradient);
    },
    changeHeatmapRadius: function(radius) {
        this.heatmap.set('radius', radius);
        this.heatmap.set('scaleRadius', true);
        this.heatmap.set('useLocalExtrema', true);
    },
    toggleMenu: function() {
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    },
    removeMenu: function() {
        $(".js-exit").click(function(e) {
            e.preventDefault();
            $("#wrapper").addClass("toggled");
        });
    },
    skrollr: function() {
        // Init Skrollr
        var s = skrollr.init({forceHeight: false,
            render: function(data) {
                //Debugging - Log the current scroll position.
                //console.log(data.curTop);
            }
        });
        $(window).on('resize', function() {
            s.destroy();
            s = skrollr.init({forceHeight: false,
                render: function(data) {
                    //Debugging - Log the current scroll position.
                    //console.log(data.curTop);
                }
            });
        });
    },
    changeSearchBUtton: function() {
        $('.live button').click(function (e) {
            e.preventDefault();
            $('.search-button').val($(this).val());
            if ($(this).val() == 1) {
                $('.search-button').removeClass('btn-success');
                $('.search-button').addClass('btn-primary');
            } else {
                $('.search-button').removeClass('btn-primary');
                $('.search-button').addClass('btn-success');
            }
            $(this).closest('.btn-group').removeClass('open');
            return false;
        });
    },
    apitabs: function() {
        $('#apitabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
            return false;
        });
    },
    visualizationtabs: function() {
        $('#visualizationtabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
            return false;
        });
    }
};

(function($) {
    latitude  = 55;
    longitude = 12;
    map_functions.init(latitude, longitude);

    /*
    $('.js-list-places-btn').click(function(e) {
        e.preventDefault();
        $('.places').toggleClass('foldout');
        return false;
    });
    */
})(jQuery);