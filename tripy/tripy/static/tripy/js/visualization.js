var visualization_distribution_functions = {
    init: function() {
        this.get_review_ratings();
        this.get_review_new_ratings();
        this.get_place_ratings();
        this.get_place_new_ratings();
    },
    get_review_ratings: function() {
        $('#visualization #review-rating-distribution .js-reviews-rating-loader').fadeIn();
        $.ajax({
            url: get_review_ratings_distribution_url,
            data: {
                'rating_type': 'rating',
                'query': query
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $('#visualization #review-rating-distribution .js-reviews-rating-loader').fadeOut();
                var chart = visualization_distribution_functions.plot_distribution(
                    data.results,
                    $('#visualization #distributed-review-rating-bar-plot'),
                    'Rating distribution',
                    '#f7464a',
                    'Number of reviews'
                );
                $('#visualizationtabs a').click(function() {
                    $(window).trigger("resize");
                });
            },
            error: function(data) { 
                global_functions.raise_alert('Error getting review ratings.');
                $('#visualization #review-rating-distribution .js-reviews-rating-loader').fadeOut();
            }
        });
    },
    get_review_new_ratings: function() {
        $('#visualization #review-rating-distribution .js-reviews-new-rating-loader').fadeIn();
        $.ajax({
            url: get_review_ratings_distribution_url,
            data: {
                'rating_type': 'new',
                'query': query
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $('#visualization #review-rating-distribution .js-reviews-new-rating-loader').fadeOut();
                var chart = visualization_distribution_functions.plot_distribution(
                    data.results,
                    $('#visualization #distributed-review-new-rating-bar-plot'),
                    'New generated rating distribution',
                    '#46f79b',
                    'Number of reviews'
                );
                $('#visualizationtabs a').click(function() {
                    $(window).trigger("resize");
                });
            },
            error: function(data) { 
                global_functions.raise_alert('Error getting new review ratings.');
                $('#visualization #review-rating-distribution .js-reviews-new-rating-loader').fadeOut();
            }
        });
    },
    get_place_ratings: function() {
        $('#visualization #place-rating-distribution .js-places-rating-loader').fadeIn();
        $.ajax({
            url: get_place_ratings_distribution_url,
            data: {
                'rating_type': 'rating',
                'query': query
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $('#visualization #place-rating-distribution .js-places-rating-loader').fadeOut();
                var chart = visualization_distribution_functions.plot_distribution(
                    data.results,
                    $('#visualization #distributed-place-rating-bar-plot'),
                    'Rating distribution',
                    '#f7464a',
                    'Number of places'
                );
                $('#visualizationtabs a').click(function() {
                    $(window).trigger("resize");
                });
            },
            error: function(data) { 
                global_functions.raise_alert('Error getting place ratings.');
                $('#visualization #place-rating-distribution .js-places-rating-loader').fadeIn();
            }
        });
    },
    get_place_new_ratings: function() {
        $('#visualization #place-rating-distribution .js-places-new-rating-loader').fadeIn();
        $.ajax({
            url: get_place_ratings_distribution_url,
            data: {
                'rating_type': 'new',
                'query': query
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $('#visualization #place-rating-distribution .js-places-new-rating-loader').fadeOut();
                var chart = visualization_distribution_functions.plot_distribution(
                    data.results,
                    $('#visualization #distributed-place-new-rating-bar-plot'),
                    'New generated rating distribution',
                    '#46f79b',
                    'Number of places'
                );
                $('#visualizationtabs a').click(function() {
                    $(window).trigger("resize");
                });
            },
            error: function(data) { 
                global_functions.raise_alert('Error getting new place ratings.');
                $('#visualization #place-rating-distribution .js-places-new-rating-loader').fadeOut();
            }
        });
    },
    plot_distribution: function(data, div, title, color, text) {
        return div.highcharts({
            chart: {
                type: 'column',
                backgroundColor:'transparent',
                type: 'column'
            },
            title: {
                text: title
            },
            xAxis: {
                categories: [
                    '1',
                    '2',
                    '3',
                    '4',
                    '5'
                ],
                title: {
                    text: 'Rating'
                }
            },
            yAxis: {
                title: {
                    text: text
                }
            },
            plotOptions: {
                column: {
                    groupPadding: 0,
                    pointPadding: 0.01,
                    borderWidth: 0
                }
            },
            series: [{
                showInLegend: false,
                color: color,
                data: data
            }]
        });
    }
};
$(function () {
    visualization_distribution_functions.init();
});