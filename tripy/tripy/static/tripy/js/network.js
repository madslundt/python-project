var network_functions = {
    init: function() {
        this.loadNetwork();
        this.loadNetwork2();
        this.changeDegree();
        $('#indegree-selector').hide();
    },
    loadNetwork: function(degree) {
        $('.network-loader').fadeIn();
        $('#network-standard').hide();
        $.ajax({
            type: "GET",
            url: get_network,
            contentType: "image/png;base64",
            data: {
                'query': query,
                'degree': degree || 1
            },
            success: function(data) {
                $('.network-loader').fadeOut();
                $('#network-standard').html('<img src="data:image/png;base64,' + data + '" />');
                $('#indegree-selector').show();
                $('#network-standard').show();
                },
            error: function(data) { 
                 global_functions.raise_alert("Could not load network");
                 $('.network-loader').fadeOut();
                 $('#indegree-selector').hide();
            }
        });
    },
    changeDegree: function() {
        $('.js-indegree').change(function () {
            network_functions.loadNetwork(this.value);
        });
    },
    loadNetwork2: function () {
        $('.network-loader2').fadeIn();
        $('#network2').hide();
        $.ajax({
            type: 'GET',
            url: get_network2,
            contentType: 'json',
            data: {
                'query': query
            },
            success: function (data) {
                $('#network2').show();
                new sigma({
                    graph: data,
                    container: 'network2',
                    settings: {
                        defaultNodeColor: '#ec5148'
                    }
                });
                $('.network-loader2').fadeOut();
            },
            error: function() {
                console.log('Error loading network2');
                $('.network-loader2').fadeOut();
                $('#network2').hide();
                global_functions.raise_alert("Could not load network");
            }
        })
    }
}


$(function () {
    network_functions.init();
});