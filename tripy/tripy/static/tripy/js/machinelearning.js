var machinelearning_functions = {
    init: function() {
        this.get_reviews();
    },
    get_reviews: function() {
        $('#machine-learning .loader').fadeIn();
        $.ajax({
            url: get_rating_errors_url,
            data: {
                'query': query
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $('#machine-learning .loader').fadeOut();
                var errors = data.errors;
                var training_error = data.training_error;
                var reviews_length = data.count;
                machinelearning_functions.plotchart(errors, training_error, reviews_length);
            },
            error: function(data) {
                global_functions.raise_alert("Getting reviews for machine learning"); 
                $('#machine-learning .loader').fadeOut();
            }
        });
    },
    plotchart: function(errors, training_error, reviews_length) {
        $('#machinelearning-piechart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                backgroundColor:'transparent'
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 50
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: false,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white',
                            textShadow: '0px 1px 2px black'
                        }
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%']
                }
            },
            colors: [
                '#46f79b',
                '#f7464a'
            ],
            series: [{
                type: 'pie',
                innerSize: '50%',
                name: 'Rating share',
                data: [
                    ['Success', (reviews_length - errors)],
                    ['Errors', errors]
                ]
            }]
        });
    }
}

$(function () {
    machinelearning_functions.init();
});