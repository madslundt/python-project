var global_functions = {
    mywindow: false,
    mypos: null,
    up: false,
    newscroll: 0,
    init: function() {
        this.mywindow = $(window);
        this.mypos = this.mywindow.scrollTop()
        $('.scroll-up').hide();
        this.scrollTop();
        this.scroll();
        this.home_sections();
        this.home_section_overlays();
        this.resizeMain();
        $('#global-alert').hide();
        $('.js-intro').click(function () { global_functions.takeIntro(); });
        this.checkIntroTime();
        $('.js-search').focus();
    },
    scrollTop: function() {
        this.mywindow.scroll(function () {
            global_functions.newscroll = global_functions.mywindow.scrollTop();
            if (global_functions.newscroll > global_functions.mypos && !global_functions.up) {
                $('.scroll-up').stop().slideToggle();
                global_functions.up = !global_functions.up;
            } else if(global_functions.newscroll < global_functions.mypos && global_functions.up) {
                $('.scroll-up').stop().slideToggle();
                global_functions.up = !global_functions.up;
            }
            global_functions.mypos = global_functions.newscroll;
        });
    },
    scroll: function() {
        $('a.scroll').click(function(e) {
            e.preventDefault();
            if ($(this).attr('href').slice(0, 1) === '#') {
                $('html, body').animate({
                    scrollTop: $($(this).attr('href')).offset().top
                }, 1000);
            }
            return false;
        });
    },
    home_sections: function() {
        $('.home-section').each(function (index) {
            global_functions.resize($(this));
        });
        $(window).on('resize', function() {
            global_functions.resizeMain();
            $('.home-section').each(function (index) {
                global_functions.resize($(this));
            });
        });
    },
    home_section_overlays: function() {
        $('.home-section .overlay').css({'opacity': '0'});
        $('#machine-learning .overlay').css({'opacity': '1'});
        $('#visualization').viewportChecker({
            classToAdd: 'visible',
            offset: 100,
            repeat: false,
            callbackFunction: function(elem, action) {
                setTimeout(function() {
                    elem.find('.overlay').css({'opacity': '1'});
                }, 500);
            }
        });
        $('#network').viewportChecker({
            classToAdd: 'visible',
            offset: 100,
            repeat: false,
            callbackFunction: function(elem, action) {
                setTimeout(function() {
                    elem.find('div .overlay').css({'opacity': '1'});
                }, 500);
            }
        });
        $('#api').viewportChecker({
            classToAdd: 'visible',
            offset: 100,
            repeat: false,
            callbackFunction: function(elem, action) {
                setTimeout(function() {
                    elem.find('.overlay').css({'opacity': '1'});
                }, 500);
            }
        });
        $('#django').viewportChecker({
            classToAdd: 'visible',
            offset: 100,
            repeat: false,
            callbackFunction: function(elem, action) {
                setTimeout(function() {
                    elem.find('.overlay').css({'opacity': '1'});
                }, 500);
            }
        });
        $('#django').viewportChecker({
            classToAdd: 'visible',
            offset: 100,
            repeat: false,
            callbackFunction: function(elem, action) {
                setTimeout(function() {
                    elem.find('.overlay').css({'opacity': '1'});
                }, 500);
            }
        });
    },
    resize: function(obj) {
        var win_height = $(window).height();
        if (obj.height() < win_height) {
            obj.height(win_height);
        }
        if ($(this).children('.container').height() > obj.height()) {
            win_height = $(this).children('.container').height();
            obj.height(win_height);
        }
        obj.css('min-height', win_height);
    },
    resizeMain: function() {
        var win_height = $(window).height();
        $('.masthead').height(win_height * 0.8);
    },
    raise_alert: function(alert_message) {
        $('.js-error-alerts').append("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> <strong>Error: </strong>" + alert_message + "<button type=\"button\" class=\"btn btn-link pull-right\" onClick=\"window.location.reload()\"><i class=\"glyphicon glyphicon-refresh\"></i></button></div>");
        $('.js-error-alerts').fadeIn();
    },
    takeIntro: function() {
        var intro = introJs();
        intro.setOptions({
            showProgress: false,
            keyboardNavigation: true,
            showStepNumbers: false,
            exitOnOverlayClick: false
        });
        intro.start();
        intro.onbeforechange(function(targetElement) {
            $("#wrapper").removeClass("toggled");
        });
        intro.onexit(function(targetElement) {
            $("#wrapper").addClass("toggled");
            $('.js-search').focus();
        });
        intro.oncomplete(function(targetElement) {
            $("#wrapper").addClass("toggled");
            $('.js-search').focus();
        });
    },
    checkIntroTime: function() {
        var introTime = this.readCookie('lastintro');
        if (introTime != 'now') {
            this.takeIntro();
            this.writeCookie('lastintro', 'now', 1);
        }
    },
    readCookie: function(name) {
        var i, c, ca, nameEQ = name + "=";
        ca = document.cookie.split(';');
        for(i=0;i < ca.length;i++) {
            c = ca[i];
            while (c.charAt(0)==' ') {
                c = c.substring(1,c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length,c.length);
            }
        }
        return '';
    },
    writeCookie: function(name, value, days) {
        var date, expires;
        if (days) {
            date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = "; expires=" + date.toGMTString();
                }else{
            expires = "";
        }
        document.cookie = name + "=" + value + ';expires=' + expires + "; path=/";
    }
}
$(function() {
    global_functions.init();
});
