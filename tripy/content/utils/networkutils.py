from content.models import Place
from itertools import chain
import requestutils
import matplotlib.pyplot as plt
import networkx as nx
from content.models import Reviews
import StringIO
import urllib, base64

class Networkutils:
	def __init__(self, places, parameter):
		self.places		=	places
		self.network 	=	self.create_network_from_places(self.places, parameter=parameter)

	# Method finding links:
	# 0 - find all links for an author (default)
	# 1 - find all good links (rating is above or equal to 3)
	# 2 - find all bad links (rating beneath 3)
	def find_links(self, place, parameter):
		reviews = requestutils.getReviewsFromPlace(place)
		author_reviews = Reviews.objects.none()
		links = []
		for review in reviews:
			if not review.author.name=='A Google User':
				author_reviews = requestutils.getReviewsFromAuthor(review.author)
				for author_review in author_reviews:
					if parameter==1: #find good links
						if author_review.rating >= 4:
							links.append(author_review.place.id)
					elif parameter==2: #find bad links
						if author_review.rating < 4:
							links.append(author_review.place.id)
					else: #find all links
						links.append(author_review.place.id)
		return links

	# Finding all links for all places
	# 0 - find all links for the places (default)
	# 1 - find all good links (rating is above or equal to 3)
	# 2 - find all bad links (rating beneath 3)
	def find_all_links(self, places, parameter):
		places_links = {}
		insert_parameter = parameter
		for place in places:
			temp_links = self.find_links(place, insert_parameter)
			if len(temp_links):
				places_links[place.id] = temp_links

		return places_links

	# Creating the network for either good, bad or all links.
	# 0 - for all links for the found places (default)
	# 1 - for all good links (rating is above or equal to 3)
	# 2 - for all bad links (rating beneath 3)
	def create_network_from_places(self, places, parameter):
		network = nx.DiGraph()
		insert_parameter = parameter
		places_links = self.find_all_links(places, insert_parameter)
		for place in places_links:
			for link_index in xrange(len(places_links[place])):
				#adding all the edges from one link to another
				network.add_edge(place, places_links[place][link_index])	
		return network

	# Plot the network and save as png to base64
	def plot_network(self, degree=1):
		plt.clf()
		plt.figure(figsize=(10,6))
		# Select places only that have some connected graphs
		indeg2 = [(n1, n2) for n1, n2 in self.network.edges() if self.network.in_degree(n1) > degree and self.network.in_degree(n2) > degree]
		graph2 = nx.DiGraph()
		# Add edges from these nodes
		graph2.add_edges_from(indeg2)
		pos = nx.spring_layout(graph2)
		nx.draw_networkx(graph2, pos, False)
		plt.axis('off')
		plt.title('Connected Places')
		imgdata = StringIO.StringIO()
		plt.savefig(imgdata, format='png', bbox_inches='tight', transparent=True)
		imgdata.seek(0)
		uri = '' + urllib.quote(base64.b64encode(imgdata.buf))
		return uri
