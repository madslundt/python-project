import os, csv, requests
from content.models import Place, Reviews
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB

class Machinelearning:
    def __init__(self, places):
        self.places          = places
        self.reviews         = Reviews.objects.filter(place__in=places)
        self.x_bow           = None
        self.y_bow           = None
        self.m_bow           = None
        self.y_hat           = None
        self.prediction_tree = None

    def generateData(self):
        if not self.places or not self.reviews:
            raise Exception('No data')

        anew = self.__anew_dictionary()
        self.m_bow = sorted(anew.keys())
        vectorizer = CountVectorizer(vocabulary=self.m_bow)
        review_texts = []
        for review in self.reviews:
            review_texts.append(review.text)

        self.x_bow = vectorizer.fit_transform(review_texts).toarray()

        y_bow = []
        for review in self.reviews:
            y_bow.append(review.rating)

        self.y_bow = np.array(y_bow)

        prediction_tree = tree.DecisionTreeClassifier()
        prediction_tree = prediction_tree.fit(self.x_bow, self.y_bow)
        self.y_hat = prediction_tree.predict(self.x_bow)

    def generateXanew(self):
        pass

    def save(self):
        if len(self.y_hat) < 1:
            return

        count = 0
        for y in self.y_hat:
            review = self.reviews[count]
            review.new_rating = y
            review.save()
            count += 1


    def predict_errors(self):
        if not self.places or not self.reviews:
            raise Exception('No data')
        def y_hat_prediction_errors(y_hat, y_bow):
            count = 0
            error_count = 0
            for y in y_hat:
                if y != y_bow[count]:
                    error_count += 1
                count += 1

                return error_count

        errors = y_hat_prediction_errors(self.y_hat, self.y_bow)
        training_error = float(errors)/float(len(self.y_hat))
        return (errors, training_error)

    def cross_validation_errors(n_ms=None, parameter=0, K=3):
        # parameter
        # 0: None
        # 1: With GaussianNB
        # 2: With MultinomialNB
        if not self.places or not self.reviews:
            raise Exception('No data')
        def k_fold_cross_validation():
            ret = []
            for k in xrange(K):
                x_test  = []
                x_train = []
                y_test  = []
                y_train = []
                count   = 0
                #creating validation and training sets for both x_bow and y_bow
                for x in self.x_bow:
                    if count % K == k:
                        x_test.append(x)
                    else:
                        x_train.append(x)
                    count += 1

                count = 0
                for y in self.y_bow:
                    if count % K == k:
                        y_test.append(y)
                    else:
                        y_train.append(y)
                    count += 1

                ret.append(
                    (
                        np.array(x_test),
                        np.array(x_train),
                        np.array(y_test),
                        np.array(y_train)
                    )
                )
            return ret

        total_error_count = 0
        validations = k_fold_cross_validation()
        for x_test, x_train, y_test, y_train in validations:
            #creating prediction tree and fitting the test data
            if n_ms:
                prediction_tree = tree.DecisionTreeClassifier(min_samples_split=n_ms)
            else:
                if parameter == 1:
                    prediction_tree = GaussianNB()
                elif parameter == 2:
                    prediction_tree = MultinomialNB()
                else:
                    prediction_tree = tree.DecisionTreeClassifier()
            prediction_tree = prediction_tree.fit(x_train, y_train)
            #creating our new y_hat
            y_hat = prediction_tree.predict(x_test)
            #comparing y_hat with y_bow
            error_count = y_hat_prediction_errors(y_hat, y_bow)
            print "%d errors in a total of %d"%(error_count, len(x_test))
            total_error_count += error_count
        
        training_error = float(total_error_count) / float(len(self.y_bow))
        return (total_error_count, training_error)

    def __anew_dictionary(self, MEAN_SUM_SUBTRACT=5.0):
        WORD_LIST_PATH    = 'ANEW.csv'
        WORD_LIST_URL     = 'http://crr.ugent.be/papers/Ratings_Warriner_et_al.csv'
        if not (os.path.exists(WORD_LIST_PATH)):
            f = open(WORD_LIST_PATH, 'w')
            response = requests.get(WORD_LIST_URL)
            f.write(response.text)
            f.close()
        f = open(WORD_LIST_PATH)
        word_list = csv.DictReader(f)

        # Reads csv and saves it in a dict with key Word and Value {V.Mean.Sum,A.Mean.Sum,D.Mean.Sum}
        anew = {}
        for row in word_list:
            anew[row['Word']] = {
                'V.Mean.Sum': float(row['V.Mean.Sum']) - MEAN_SUM_SUBTRACT,
                'A.Mean.Sum': float(row['A.Mean.Sum']) - MEAN_SUM_SUBTRACT,
                'D.Mean.Sum': float(row['D.Mean.Sum']) - MEAN_SUM_SUBTRACT
            }
        f.close()
        return anew