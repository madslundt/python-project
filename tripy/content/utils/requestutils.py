from django.conf import settings
import requests
import re
from django.db.models import Q
from content.models import Author, Place, Reviews, Country, PostalCode

def getLocalPlaces(query=''):

    split = re.findall(r'[,\w]+', query)
    array = []
    for s in split:
        s = s.replace(',', '').strip()
        if len(s) > 2 and not s.isdigit():
            array.append(s)
    if array and len(array) > 0:
        regex = '^.*(%s).*$' % '|'.join(array)
        places = Place.objects.filter(
            Q(id__icontains=query) |
            Q(name__icontains=query) |
            Q(postal__postal__icontains=query) |
            Q(postal__locality__icontains=query) |
            Q(postal__country__short_name__icontains=query) |
            Q(postal__country__name__icontains=query) |
            Q(id__iregex=regex) |
            Q(name__iregex=regex) |
            Q(postal__postal__iregex=regex) |
            Q(postal__locality__iregex=regex) |
            Q(postal__country__short_name__iregex=regex) |
            Q(postal__country__name__iregex=regex)
        )
    else:
        places = Place.objects.filter(
            Q(id__icontains=query) |
            Q(name__icontains=query) |
            Q(postal__postal__icontains=query) |
            Q(postal__locality__icontains=query) |
            Q(postal__country__short_name__icontains=query) |
            Q(postal__country__name__icontains=query)
        )
    return places

def getPlaces(query=''):
    url = 'https://maps.googleapis.com/maps/api/place/textsearch/json'

    params = {
        'query': query,
        'key': settings.GOOGLE_PLACES_API,
        'language': settings.LANGUAGE_CODE
    }
    json = getJson(url, params)
    if not json:
        raise Exception('Connection Error')
    elif json['status'] != 'OK':
        raise Exception(json['error_message'])

    savePlaces(json['results']) # TODO run async
    while True: #for i in xrange(0, 10):
        if not 'next_page_token' in json.keys():
            break
        params['pagetoken'] = json['next_page_token']
        json = getJson(url, params)
        if not json:
            raise Exception('Connection Error')
        elif json['status'] != 'OK':
            raise Exception(json['error_message'])
        savePlaces(json['results']) # TODO run async

    return json['results']

def getNearbyPlaces(lat, lng):
    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=AddYourOwnKeyHere'
    # TODO if necessary

def getPlace(placeid):
    url = 'https://maps.googleapis.com/maps/api/place/details/json'

    params = {
        'placeid': placeid,
        'key': settings.GOOGLE_PLACES_API,
        'language': settings.LANGUAGE_CODE
    }
    json = getJson(url, params)
    if json['status'] != 'OK':
        raise Exception(json['error_message'])

    return json['result']

def getJson(url, params):
    request = requests.get(url, params=params)
    if request.status_code != 200:
        return False
    
    json = request.json()
    return json

def getReviewsFromPlaces(places):
    reviews = Reviews.objects.filter(place__in=places)
    return reviews

def getReviewsFromPlace(place):
    reviews = Reviews.objects.filter(place=place)
    return reviews

def getReviewsFromAuthor(author):
    reviews = Reviews.objects.filter(author=author)
    return reviews

def getReviewsFromAuthors(author):
    reviews = Reviews.objects.filter(author__in=author)
    return reviews

def savePlaces(places):
    if places:
        for place in places:
            p = getPlace(place['place_id'])
            savePlace(p)

def savePlace(place):
    if not 'reviews' in place.keys() or len(place['reviews']) < 1:
        return
    place_id    = place['place_id']
    place_name  = place['name']
    postal_obj = None

    if 'address_components' in place.keys():
        postal_code = None
        country = None
        locality = None
        address_components = place['address_components']
        for a in address_components:
            if 'postal_code' in a['types']:
                postal_code = a
            elif 'country' in a['types']:
                country = a
            elif 'locality' in a['types']:
                locality = a

        if country and postal_code:
            country_obj = Country.objects.get_or_create(short_name=country['short_name'], defaults={'name': country['long_name']})
            if locality:
                postal_obj  = PostalCode.objects.get_or_create(postal=postal_code['long_name'], country=country_obj[0], defaults={'locality': locality['long_name']})
            else:
                postal_obj  = PostalCode.objects.get_or_create(postal=postal_code['long_name'], country=country_obj[0])
        else:
            return
    else:
        return

    avg_rating  = None
    if 'rating' in place.keys():
        avg_rating  = place['rating']

    price_level = None
    if 'price_level' in place.keys():
        price_level = place['price_level']

    geo_lat = None
    geo_lng = None
    if 'geometry' in place.keys():
        geo_lat = place['geometry']['location']['lat']
        geo_lng = place['geometry']['location']['lng']
    
    place_obj = Place.objects.get_or_create(id=place_id, 
        defaults={
            'name': place_name, 
            'price_level': price_level,
            'avg_rating': avg_rating,
            'postal': postal_obj[0],
            'geo_lat': geo_lat,
            'geo_lng':geo_lng
        })
    place_obj  = place_obj[0]
    reviews = place['reviews']
    for review in reviews:
        if not 'language' in review.keys() or not review['language'] == 'en':
            continue

        text        = review['text']
        rating      = review['rating']
        author_name = review['author_name']
        author_id   = author_name
        if 'author_url' in review.keys():
            author_id   = review['author_url'].split('/')[-1]

        author_obj  = Author.objects.get_or_create(id=author_id, 
            defaults={'name': author_name})
        
        author_obj = author_obj[0]

        Reviews.objects.get_or_create(place=place_obj, author=author_obj, 
            defaults={'text': text, 'rating': rating})