from django.db import models

class Country(models.Model):
    short_name = models.CharField(unique=True, primary_key=True, max_length=10)
    name       = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'countries'

class PostalCode(models.Model):
    postal   = models.CharField(blank=False, null=False, max_length=15)
    country  = models.ForeignKey(Country, blank=False, null=False)
    locality = models.CharField(blank=True, null=True, max_length=100)

    def __str__(self):
        return self.postal

    def __unicode__(self):
        return self.postal

    class Meta:
        unique_together = ("postal", "country")

class Author(models.Model):
    id = models.CharField(unique=True, max_length=100, primary_key=True)
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class Place(models.Model):
    id          = models.CharField(unique=True, max_length=100, primary_key=True)
    name        = models.CharField(max_length=100)
    price_level = models.FloatField(blank=True, null=True)
    avg_rating  = models.FloatField(blank=True, null=True)
    geo_lat     = models.FloatField(blank=True, null=True)
    geo_lng     = models.FloatField(blank=True, null=True)
    postal      = models.ForeignKey(PostalCode, null=True)

    def __str__(self):
        return self.name
    
    def __unicode__(self):
        return self.name

class Reviews(models.Model):
    place = models.ForeignKey(Place)
    author = models.ForeignKey(Author)
    text  = models.TextField(max_length=600)
    rating = models.PositiveSmallIntegerField(null=False)
    new_rating = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.place.name
    
    def __unicode__(self):
        return self.place.name

    class Meta:
        verbose_name_plural = "reviews"