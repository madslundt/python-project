from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Q
import json, models
from django.db.models import Count
from utils import requestutils, machinelearningutils, networkutils
from content.models import Place, Reviews, Author
import os
from os.path import abspath, dirname
from django.db.models import Avg
from django.http import Http404
import time, math
import operator

def robots(request):
    txt = """User-agent: *
    Disallow: /"""
    return HttpResponse(txt, content_type='text/plain')

def home(request):
    places          = None
    error           = None
    query           = ''
    live            = False
    errors          = 0

    if 'query' in request.GET.keys():
        query = request.GET['query']
        if request.user.is_authenticated() and 'live' in request.GET.keys() and request.GET['live'] == '1':
            live = True
            try:
                places = requestutils.getPlaces(query)
                requestutils.savePlaces(places)
                places_local = requestutils.getLocalPlaces(query)
                if places_local:
                    ml = machinelearningutils.Machinelearning(places_local)
                    ml.generateData()
                    ml.save()
            except Exception as err:
                error = err.args[0]
        #else:
        #    places = requestutils.getLocalPlaces(query).annotate(num_reviews=Count('reviews')).annotate(Avg('reviews__new_rating'))
        #    reviews = requestutils.getReviewsFromPlaces(places).order_by('-rating')

        #    if reviews:
        #        for review in reviews:
        #            if review.rating != review.new_rating:
        #                errors += 1
        #        training_error = float(errors)/float(len(reviews)) * 100 # To get percentage

            # creating and saving network of places
            # network = networkutils.Networkutils(places)
            # network_picture = network.plot_network()

    place_count = Place.objects.all().count()
    review_count = Reviews.objects.all().count()
    author_count = Author.objects.all().count()

    return render(request, 'content/home.html', {
        'places': places,
        'error': error,
        'query': query,
        'live': live,
        'errors': errors,
        'total_places_count': place_count,
        'total_reviews_count': review_count,
        'total_authors_count': author_count
    })

def about(request):
    return render(request, 'content/about.html', {})

def get_map_places(request):
    if not 'query' in request.GET.keys():
        raise Http404

    start_time = time.time()
    query = request.GET['query']
    places = requestutils.getLocalPlaces(query).annotate(num_reviews=Count('reviews')).annotate(review_avg_new_rating=Avg('reviews__new_rating'))
    data = {
        'count': len(places),
        'results': []
    }
    for place in places:
        postal = None
        try:
            if place.postal:
                postal = place.postal.postal
        except:
            pass
        rating = 0
        if place.review_avg_new_rating:
            rating = place.review_avg_new_rating
        data['results'].append({
            'name': place.name,
            'price_level': place.price_level,
            'new_avg_rating': rating,
            'geo_lat': place.geo_lat,
            'geo_lng': place.geo_lng,
            'postal': postal
        })
    data['time'] = time.time() - start_time

    data = json.dumps(data)

    return HttpResponse(data, content_type='application/json')

def get_machinelearning_data(request):
    if not 'query' in request.GET.keys():
        raise Http404

    start_time = time.time()
    query = request.GET['query']
    places = requestutils.getLocalPlaces(query).annotate(review_avg_new_rating=Avg('reviews__new_rating')).annotate(review_avg_rating=Avg('reviews__rating'))
    data = {
        'count': len(places),
        'results': []
    }
    for place in places:
        rating = 0
        if place.review_avg_new_rating:
            rating = place.review_avg_new_rating
        data['results'].append({
            'name': place.name,
            'price_level': place.price_level,
            'new_avg_rating': rating,
            'geo_lat': place.geo_lat,
            'geo_lng': place.geo_lng,
            'avg_rating': reviews__rating
        })
    data['time'] = time.time() - start_time

    data = json.dumps(data)

    return HttpResponse(data, content_type='application/json')

def get_rating_errors(request):
    if not 'query' in request.GET.keys():
        raise Http404

    start_time = time.time()
    query = request.GET['query']
    places = requestutils.getLocalPlaces(query).annotate(num_reviews=Count('reviews')).annotate(Avg('reviews__new_rating'))
    reviews = requestutils.getReviewsFromPlaces(places).order_by('-rating')

    errors = 0
    training_error = 0
    data = {
        'count': len(reviews),
        'errors': 0,
        'training_error': 0
    }
    if reviews:
        for review in reviews:
            if review.rating != review.new_rating:
                errors += 1
        training_error = float(errors)/float(len(reviews)) * 100 # To get percentage
    data['errors'] = errors
    data['training_error'] = training_error
    data['time'] = time.time() - start_time

    data = json.dumps(data)

    return HttpResponse(data, content_type='application/json')

def get_reviews_data(request):
    if not 'query' in request.GET.keys():
        raise Http404

    start_time = time.time()
    query = request.GET['query']
    places = requestutils.getLocalPlaces(query).annotate(num_reviews=Count('reviews')).annotate(Avg('reviews__new_rating'))
    reviews = requestutils.getReviewsFromPlaces(places).order_by('-rating')

    errors = 0
    training_error = 0
    data = {
        'count': len(reviews),
        'errors': 0,
        'training_error': 0,
        'results': []
    }
    if reviews:
        for review in reviews:
            if review.rating != review.new_rating:
                errors += 1
            data['results'].append({
                'place': review.place.name,
                'author': review.author.name,
                'text': review.text,
                'rating': review.rating,
                'new_rating': review.new_rating
            })
        training_error = float(errors)/float(len(reviews)) * 100 # To get percentage
    data['errors'] = errors
    data['training_error'] = training_error
    data['time'] = time.time() - start_time

    data = json.dumps(data)

    return HttpResponse(data, content_type='application/json')

def get_network(request):
    if not 'query' in request.GET.keys():
        raise Http404

    degree = 1
    if 'degree' in request.GET.keys():
        degree = int(request.GET['degree'])

    if degree > 5:
        degree = 5
    elif degree < 1:
        degree = 1

    start_time = time.time()
    query = request.GET['query']
    places = requestutils.getLocalPlaces(query).annotate(num_reviews=Count('reviews')).annotate(Avg('reviews__new_rating'))


    network = networkutils.Networkutils(places, parameter=0)
    network_picture = network.plot_network(degree)

    return HttpResponse(network_picture, content_type='image/png;base64')

def get_network2(request):
    if not 'query' in request.GET.keys():
        raise Http404

    query = request.GET['query']
    places = requestutils.getLocalPlaces(query)
    reviews = requestutils.getReviewsFromPlaces(places)
    place_sizes = {}

    nodes = []
    edges = []
    authors = {}

    for review in reviews:
        if review.author.name == 'A Google User':
            continue
        authors[review.author.name] = authors.get(review.author.name, [])
        authors[review.author.name].append(review.place.id)
        place_sizes[review.place.id] = place_sizes.get(review.place.id, 0) + 1

    top100 = sorted(place_sizes.items(), key=operator.itemgetter(1), reverse=True)[:100]
    top = [n[0] for n in top100]
    
    counter = 0

    for place in places:
        if not place.id in top:
            continue
        nodes.append({
            'id': place.id,
            'label': place.name,
            'x': math.cos(math.pi * 2 * counter / 100),
            'y': math.sin(math.pi * 2 * counter / 100),
            'size': place_sizes[place.id]
        })
        counter += 1


    counter = 0
    for key, value in authors.iteritems():
        old_place = None
        for place in value:
            if not place in top:
                continue
            if old_place:
                edges.append({
                    'id': str(counter),
                    'source': old_place,
                    'target': place
                })
            old_place = place
            counter += 1
    data = {
        'nodes': nodes,
        'edges': edges
    }
    data = json.dumps(data)

    return HttpResponse(data, content_type='application/json')

def get_review_rating_distribution(request):
    if not 'rating_type' in request.GET.keys() or not 'query' in request.GET.keys():
        raise Http404

    start_time = time.time()
    rating_type = request.GET['rating_type']
    query = request.GET['query']
    places = requestutils.getLocalPlaces(query)
    r_type = 'rating'
    if rating_type == 'new':
        r_type = 'new_rating'
    reviews = requestutils.getReviewsFromPlaces(places).order_by(r_type)

    results = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}
    for review in reviews:
        if r_type == 'rating':
            r = review.rating
        else:
            r = review.new_rating
        if r:
            results[r] += 1
    data = {
        'count': len(reviews),
        'results': results.values(),
        'time': time.time() - start_time
    }
    data = json.dumps(data)

    return HttpResponse(data, content_type='application/json')

def get_place_rating_distribution(request):
    if not 'rating_type' in request.GET.keys() or not 'query' in request.GET.keys():
        raise Http404

    start_time = time.time()
    rating_type = request.GET['rating_type']
    query = request.GET['query']
    places = requestutils.getLocalPlaces(query)
    
    if rating_type == 'new':
        places = places.annotate(rating_avg=Avg('reviews__new_rating'))
    else:
        places = places.annotate(rating_avg=Avg('reviews__rating'))
    
    places = places.order_by('rating_avg')
    results = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}
    for place in places:
        r = place.rating_avg
        if r:
            r = math.floor(place.rating_avg)
            results[int(r)] += 1

    data = {
        'count': len(places),
        'results': results.values(),
        'time': time.time() - start_time
    }
    data = json.dumps(data)

    return HttpResponse(data, content_type='application/json')
