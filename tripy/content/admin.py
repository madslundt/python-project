from django.contrib import admin
from content.models import Author, Reviews, Place

class ReviewsInline(admin.TabularInline):
    model = Reviews
    can_delete = False
    extra = 0
    editable_fields = []
    
    def has_add_permission(self, request):
        return False

class AuthorAdmin(admin.ModelAdmin):
    readonly_fields = [
        'id',
        'name'
    ]

    list_display = ['name']
    search_fields = ['name']

    inlines = [ReviewsInline]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class ReviewsAdmin(admin.ModelAdmin):
    model = Reviews
    readonly_fields = [
        'place_name',
        'author_name',
        'text',
        'rating',
        'new_rating'
    ]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    list_display = ['place_name', 'author_name', 'rating', 'new_rating']
    search_fields = ['place_name', 'author_name']   

    def author_name(self, instance):
        return instance.author.name
    author_name.admin_order_field  = 'author'
    author_name.short_description = 'Author Name'

    def place_name(self, instance):
        return instance.author.name
    place_name.admin_order_field  = 'place'
    place_name.short_description = 'Place Name'

class PlaceAdmin(admin.ModelAdmin):
    inlines = [ReviewsInline]

    readonly_fields = [
        'id',
        'name',
        'price_level',
        'avg_rating'
    ]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    list_display = ['name', 'price_level', 'avg_rating']
    search_fields = ['name', 'price_level', 'avg_rating']

admin.site.register(Author, AuthorAdmin)
admin.site.register(Reviews, ReviewsAdmin)
admin.site.register(Place, PlaceAdmin)