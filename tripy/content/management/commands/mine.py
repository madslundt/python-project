from django.core.management.base import BaseCommand, CommandError
import time, os

# Remember to specify places.txt and completed.txt
class Command(BaseCommand):
    def handle(self, *args, **options):
        def load_src(name, fpath):
            import imp
            p = fpath if os.path.isabs(fpath) \
                else os.path.join(os.path.dirname(__file__), fpath)
            return imp.load_source(name, p)
        load_src("requestutils", "../../utils/requestutils.py")
        import requestutils
        load_src("machinelearningutils", "../../utils/machinelearningutils.py")
        import machinelearningutils
        PLACES_FILE = os.path.join(os.path.dirname(__file__), 'places.txt')
        COMPLETED_FILE = os.path.join(os.path.dirname(__file__), 'completed.txt')
        if not os.path.isfile(PLACES_FILE):
            self.stdout.write('You need to specify atleast one place (%s)'%PLACES_FILE)
            return
        if not os.path.isfile(COMPLETED_FILE):
            open(COMPLETED_FILE, 'w+')

        f = open(PLACES_FILE,"r")

        queries = [line.strip() for line in f]
        f.close()
        f = open(COMPLETED_FILE,"r")
        completed = [line.strip() for line in f]
        f.close()
        self.stdout.write('Starting..')
        self.stdout.write('%d queries'%len(queries))
        for query in queries:
            if (query in completed):
                continue

            self.stdout.write(query)
            try:
                places = requestutils.getPlaces(query)
                requestutils.savePlaces(places)
                places_local = requestutils.getLocalPlaces(query)
                if places_local:
                    ml = machinelearningutils.Machinelearning(places_local)
                    ml.generateData()
                    ml.save()
                completed.append(query)
                f = open(COMPLETED_FILE,"a")
                f.write(query + "\n")
                f.close()
            except Exception as err:
                for e in err.args:
                    self.stdout.write(e)
                break


        if len(completed) == queries:
            self.stdout.write('Please add new places to %s'%PLACES_FILE)
        self.stdout.write('Finished. Please run me later')